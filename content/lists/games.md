+++
title = "Games"
description = "A list of apps that work on Linux Phones like the PinePhone or Librem 5."
date = 2021-12-28T07:00:00+00:00
template = "lists/games.html"
draft = false

[extra]
lead = 'The LINMOBapps Game list'
images = []
+++

This is the games list, which is currently maintained by Moxvallix — Contributions to the list are welcome.

See also: Patched games repository: [https://github.com/moxvallix/LinMobGames](https://github.com/moxvallix/LinMobGames)

### Legend/Field Explaination

*   **name:** name of the app
*   **category:** App category (one or two words)
*   **reporter:** Credit to whoever reported that the app works
*   **verified:** App has been tested on a Linux mobile, and declared working (touch, ui etc) by a maintainer.
*   **summary:** one or two sentences about the app - normally a quote from the project site
*   **mobile compatibility:** a number between 5 (best) and 0 (worst) to name the mobile compatibility of the app (this is different from MGLApps to be closer to the ratings in [Mobian Wiki](https://wiki.mobian-project.org/doku.php?id=apps); category 3 may be redefined, as it includes apps that apparently have not been tested on mobile GNU/Linux or postmarketOS)
    *   5 = perfect - nothing left to be done
    *   4 = almost perfect, works fine with tweaks for scaling, like \`scale-to-fit´ on Phosh
    *   3 = some parts of the app are not usable with touch input on small screens
    *   2 = many parts of the app are not usable with touch input on small screens
    *   1 = app is unusable with touch input on small screens – why is this app in the LINMOBapps app list?
    *   A = mobile compatibility could be perfect because the app was ported to other mobile platforms (e.g. Android)
*   **repository:** link to the code repository
*   **framework:** UI frameworks which are used by the app
*   **screenshots:** links to sites providing screenshots of the application
*   **website:** link to the project‘s website
*   **license:** license of the project
*   **appid:** AppStream ID in the form {tld}.{vendor}.{product} (e.g. org.kde.itinerary)
*   **scale-to-fit:** AppID to scale the app properly in Phosh (with scale-to-fit AppID on). To find out these IDs, use [get-app-id.sh](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/get-app-id.sh) or see [linmob.net](https://linmob.net/2020/07/27/pinephone-daily-driver-challenge-part2-flatpak-and-scaling-in-phosh.html#manual-scaling-in-phosh)
*   **flatpak:** link to the Flathub page (if available) or another location the app could be installed from as flatpak. For help with installing Flatpaks see [linmob.net](https://linmob.net/2020/07/27/pinephone-daily-driver-challenge-part2-flatpak-and-scaling-in-phosh.html#flatpaks-across-pinephone-distributions)
*   **aur:** name of the corresponding package build in the [Arch User Repository](https://aur.archlinux.org). For help with the AUR see [linmob.net](https://linmob.net/2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html#preparations)
*   **postmarketos:** name of the corresponding package in the postmarketOS/Alpine repositories _(definitely incomplete, contributions welcome!)_
*   **debian:** name of the corresponding debian package _(definitely incomplete, contributions welcome!)_
*   **notice:** additional information about develpment stage, mobile compatibility or what else is important to know
*   **more information:** links to sites providing more information about the app
*   **backend:** backends which are used by the app
*   **service:** services the app includes and makes use of
*   **description:** more detailed text about the app - normally a quote from the project site
*   **description source:** if 'description' is a quote, this provides the source
*   **summary source:** if 'summary' is a quote, this provides the source

### Get in touch

If you want to contact Moxvallix, the games list maintainer, you can contact him on matrix at [moxvallix:matrix.org](https://matrix.to/#/@moxvallix:matrix.org), or send him an email at [moxvallix@gmail.com](mailto:moxvallix@gmail.com).


### Credits and Liability

The content of LinuxPhoneApps.org is LinuxPhoneApps.org is, just like its origins, the [LINMOBapps](https://framagit.org/linmobapps/linmobapps.frama.io) app list and [MGLApps](https://mglapps.frama.io) app list, licensed under [CC BY-SA 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Built with ♥ and amazing free and open source software:
* [Pivot.js](http://rwjblue.github.io/pivot.js/),
* [Datatables](https://www.datatables.net/),
* [Linkify](https://soapbox.github.io/linkifyjs/),
* [Bulma](http://bulma.io/),
* [Bulma-Extensions](https://wikiki.github.io) and
* [jQuery](https://jquery.com/)

The LinuxPhoneApps.org contributors do not take any warranty for the correctness of any information on this page. If you want to be sure, please visit the mentioned and linked project sites and check the information. The fields "summary" and "description" are quotations from the sites mentioned in the fields "summary source" and "description source".
