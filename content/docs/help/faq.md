+++
title = "FAQ"
description = "Answers to frequently asked questions."
date = 2021-08-14T18:30:00+02:00
updated = 2022-04-07T19:30:00+02:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Answers to frequently asked questions."
toc = true
top = false
+++

## What is LinuxPhoneApps?

LinuxPhoneApps.org is the continuation of [LINMOBapps](https://framagit.org/linmobapps/linmobapps.frama.io) (and thus also [MGLapps](https://mglapps.frama.io)). It lists and documents apps for Linux Phones like the PinePhone or the Librem 5 that don't have a centralized app store (yet) and run distributions such as Arch Linux ARM, Fedora Mobility, Mobian, openSUSE, postmarketOS, PureOS or similar which use Phosh, Plasma Mobile or Sxmo for their UI.

If you don't you have questions about the contents of our app listings or have suggestions for improvements (especially regarding visuals and wording), please [get in touch](#join-the-discussion-and-stay-informed) so that we can make things better and easier to grasp.

### Can I install these apps on Ubuntu Touch?

Some, maybe. Generally, Ubuntu Touch is not in scope for this project, as it has an [app store](https://open-store.io/). 

### Can I install these apps on Sailfish OS?

Some, maybe. Generally, Sailfish OS is not in the scope of this project as it has more than one app store already:
* the official Jolla App Store (no website),
* [OpenRepos.net](https://openrepos.net/) and
* [SailfishOS:Chum community repository](https://build.sailfishos.org/project/show/sailfishos:chum).

## Join the effort

The various parts of this project live under the [LinuxPhoneApps Organization on Framagit](https://framagit.org/linuxphoneapps/).

### Repositories

Currently, these are: 
* the repository for the [main page, including docs](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io),
* the old LINMOBapps repo [where our csv files LINMOBapps live on](https://framagit.org/linmobapps/linmobapps.frama.io),
* a seperate repo for the [bad Python code that turns the csv into the markdown files Zola then renders](https://framagit.org/linuxphoneapps/appscsv2tomlmd).

### Bugtrackers
If you want to work on known issues, you'll find
* [app/games content centric issues (list overhauls and such) in the LINMOBapps repo](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues),
* [other content (documentation, landing page, feeds) and all Zola related website design (listing and app overview design) issues in the LinuxPhoneApps repo](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues),
* [csv -> markdown converter centric issues csv -> markdown converter (Python) issues in its repo](https://framagit.org/linuxphoneapps/appscsv2tomlmd/-/issues).

Creating new issues is a very valuable contribution too!


### Adding or editing app listings

If you want to add an app, you can do so by forking the LINMOBapps repo and creating a merge request or sending an email. The following pages have more info on this:
* [Submit an app by email](@/docs/contributing/submit-app-by-email.md),
* [Submit an app on GitLab](@/docs/contributing/submit-app-on-gitlab.md),
* [Editing CSV in LibreOffice](@/docs/contributing/edit-csv-in-libreoffice/index.md). 

If just want to report that something is wrong with a listing, click the link at the bottom of the apps page to send an email.

### Reuse the data

If you respect the license and credit the project properly, feel free to reuse apps.csv data! Up to this point, the following projects are known to reuse .csv data:

* [LINMOBappsBrowser](https://github.com/Speiburger/LINMOBappsBrowser)
* [Simple Applist](https://framagit.org/1peter10/simple-applist)

If you have a project that reuses the data, please make sure to list your project here, too!


## Join the discussion and stay informed

Join our [Matrix room](https://matrix.to/#/#linuxphoneapps:matrix.org)!

Or, subscribe to or join the discussion one of our mailing lists:
* [Discuss](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss) list for the discussion of apps and general topics,
* [Devel](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel) list for discussion regarding further development of the LinuxPhoneApps app list.

You can also subscribe to a number of [Atom](https://en.wikipedia.org/wiki/Atom_%28Web_standard%29) feeds:
* [Apps feed](https://linuxphoneapps.org/apps/atom.xml) to learn about newly added apps,
* [Blog feed](https://⁨linuxphoneapps.org/blog/atom.xml) to learn about new blog posts.

An "updated app listings" feed as well as a feed to follow updates to this documentation are being planned and will be listed here once they have been implemented.

## Keyboard shortcuts for search? 

<mark>Currently non working, will be restored</mark>
- focus: `/`
- select: `↓` and `↑`
- open: `Enter`
- close: `Esc`
