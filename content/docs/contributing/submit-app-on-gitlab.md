+++
title = "Submit apps on GitLab"
description = "Adding or editing existing listings"
date = 2022-04-07T19:30:00+02:00
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

### Introduction

While the overall plan is to make app listings editable directly (such as this page is by its link on the button), for maintenance purposes this has not been enabled yet. We also want third parties to build upon data MGLapps, LINMOBapps and now LinuxPhoneApps.org contributors have amassed, and for that the .csv lists have to stay.

### Getting the file

All content regarding apps or games lives in the [LINMOBapps repository](https://framagit.org/linmobapps/linmobapps.frama.io). 

To add an app, just [open the apps.csv file directly](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/apps.csv), and click "Open in Web IDE".

Now you'll be asked to login into framagit. You can use your existing GitHub, Gitlab.com or Bitbucket account to speed that procedure up, if you want to. Afterwards, the repo will be forked and you edit the file locally in GitLab.

In order to add an app, you can just try to write csv by hand - it's all comma seperated after all, but given the number of columns our list has, it's not really fun to do so.

Therefore, we recommend cloning the forked repo on your local machine and editing the [file in LibreOffice](@/docs/contributing/edit-csv-in-libreoffice/index.md). If you don't want to setup SSH keys and run git locally (or can't for some reason), you can also download the file, edit it locally and just reupload it after you've edited it. After reuploading with a commit message, you just need to create a merge request to LINMOBapps, and it should be accepted shortly.
