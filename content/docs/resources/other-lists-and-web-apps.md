+++
title = "Other Lists and Web Apps"
description = "Friendly coexistence, not competition."
date = 2022-04-06T22:30:00+02:00
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

## Other lists

Aside from the origin of this list, [MGLapps](https://mglapps.frama.io), other projects have collected their own app or games lists, that are usually distribution specific.

### Mobian
* [Mobian Wiki: Apps](https://wiki.mobian.org/doku.php?id=apps)
* [Mobian Wiki: Wishlist](https://wiki.mobian.org/doku.php?id=wishlist)

### postmarketOS
* [postmarketOS Wiki: Potential apps](https://wiki.postmarketos.org/wiki/Potential_apps)
* [postmarketOS Wiki: Applications by category](https://wiki.postmarketos.org/wiki/Applications_by_category)

### PureOS
* [PureOS: Mobile-optimized apps](https://tracker.pureos.net/w/pureos/mobile_optimized_apps/)
* [PureOS: Mobile-optimized apps from 3rd party repos](https://tracker.pureos.net/w/pureos/3rd-party_mobile_optimized_apps/)
* [Purism: List of Apps in Development](https://source.puri.sm/Librem5/community-wiki/-/wikis/List-of-Apps-in-Development)
* [Purism Forums: List of Apps that fit and function well](https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well/11361)
* [Purism: Librem5: Apps gIssues](https://source.puri.sm/Librem5/Apps_Issues)

### Flathub
* [Flathub on Mobile](https://github.com/tchx84/flathub-mobile), a list of mobile Flathub apps.

### Openmoko
* [Openmoko Wiki: Applications](http://wiki.openmoko.org/wiki/Applications).

### App Stores/Listings:

* [Open Store: Ubuntu Touch Apps](https://open-store.io/)
* [Open Repos.net: Open Source Apps for Maemo, Harmattan, Sailfish OS, Nemo Mobile and PureOS](https://openrepos.net/)


## Web Apps

Besides all varieties of Linux apps, web apps are a thing. The Mobian Wiki has some [tips on how to make use of websites](https://wiki.mobian.org/doku.php?id=webapps) on Linux Phones.

### Matrix Web Apps
* [Hydrogen](https://hydrogen.element.io/#/login)
* [Cinny.in](https://cinny.in/)

### Mastodon 
* [Pinafore](https://pinafore.social/)

_If you use another web app or know another collection of apps, [please get in touch](@/docs/help/faq.md#join-the-discussion-and-stay-informed)!_


