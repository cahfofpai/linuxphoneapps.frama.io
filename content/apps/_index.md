+++
title = "Apps"
description = "A list of apps that work on Linux Phones like the PinePhone or Librem 5."
date = "2022-04-14"
sort_by = "title"
template = "apps/section.html"
page_template = "apps/page.html"
generate_feed = true
in_search_index = true
+++

 Don't like new things? Don't worry, [the old JavaScript-heavy listing is still available](@/lists/apps-classic.md)!

